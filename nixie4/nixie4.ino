#include "Arduino.h"
#include <SoftwareSerial.h>
SoftwareSerial mySerial(15, 14); // RX, TX

//  Current revisions by BRAD L - 1/31/2013

// fading transitions sketch for 4-tube board with default connections.
// based on 6-tube sketch by Emblazed
// 4-tube-itized by Dave B. 16 June 2011
// this shows minutes and seconds only

// SN74141 : Truth Table
//D C B A #
//L,L,L,L 0
//L,L,L,H 1
//L,L,H,L 2
//L,L,H,H 3
//L,H,L,L 4
//L,H,L,H 5
//L,H,H,L 6
//L,H,H,H 7
//H,L,L,L 8
//H,L,L,H 9

// lookups array of mapping from our wiring numbers to our integer values
int lookups[10] = {
  8,9,0,1,2,3,4,5,6,7,
};

long runTime = 0;       // Time from when we started.
bool minuteIncremented = false; // potentially a dumb way to make sure the increment is like a clock instead of like a crazy person


const int number0a = 2;                
const int number0b = 3;
const int number0c = 4;
const int number0d = 5;

const int number1a = 6;                
const int number1b = 7;
const int number1c = 8;
const int number1d = 9;

const int tube1 = 10;
const int tube2 = 11;
const int tube3 = 12;
const int tube4 = 13;

int numberArray[4] = { 1, 2, 3, 4 };
int pinArray[4] = { tube1, tube2, tube3, tube4 };
uint8_t count = 0;
// enDelay (ms) to hold a lamp to get some light
int enDelay = 10;
long split = 500;
long lastloop = millis() - split;

int forcedVal = 0;

typedef struct {
  int hours = 22;
  int mins = 14;
  int secs = 0;
} timeValue;

timeValue currentTime;

bool DEBUG = true;

void loop()     
{
  checkSerialClock();
  runTime = millis();
  long seconds  = (runTime / 1000) % 60;

  if (seconds == 0 && !minuteIncremented) {
    currentTime.mins ++;
    minuteIncremented = true;
    if (currentTime.mins > 59) currentTime.hours++;
  }
  else {
    minuteIncremented = false;
  }

  // Little johny tables
  currentTime.hours = currentTime.hours % 12; // 12 hour time
  // currentTime.hours = currentTime.hours % 24; // 24 hour time
  currentTime.mins = currentTime.mins  % 60;
  currentTime.secs = currentTime.secs  % 60;

  showTime();
  
}

void showTime()
{
  // int hourTens = currentTime.hours / 10;
  // hourTens = hourTens == 2 ? 5 : hourTens;
  // displayNumbers(0, hourTens, currentTime.hours % 10);
  displayNumbers(0, currentTime.hours / 10, currentTime.hours % 10);
  displayNumbers(1, currentTime.mins / 10, currentTime.mins % 10);
}

void setup() 
{
  mySerial.begin(19200); 
  
  pinMode(number0a, OUTPUT);
  pinMode(number0b, OUTPUT);
  pinMode(number0c, OUTPUT);
  pinMode(number0d, OUTPUT);
  
  pinMode(number1a, OUTPUT);
  pinMode(number1b, OUTPUT);
  pinMode(number1c, OUTPUT);
  pinMode(number1d, OUTPUT);
  
  pinMode(tube1, OUTPUT);
  pinMode(tube2, OUTPUT);
  pinMode(tube3, OUTPUT);
  pinMode(tube4, OUTPUT);

  Serial.begin(9600);
}

void displayNumbers(int set, int number1, int number2)
{
  int a1, b1, c1, d1, a2, b2, c2, d2;
  
  // tricky truth tables
  switch(lookups[number1])
  {
    case 0:   a1=0;  b1=0;  c1=0;  d1=0;  break;
    case 1:   a1=1;  b1=0;  c1=0;  d1=0;  break;
    case 2:   a1=0;  b1=1;  c1=0;  d1=0;  break;
    case 3:   a1=1;  b1=1;  c1=0;  d1=0;  break;
    case 4:   a1=0;  b1=0;  c1=1;  d1=0;  break;
    case 5:   a1=1;  b1=0;  c1=1;  d1=0;  break;
    case 6:   a1=0;  b1=1;  c1=1;  d1=0;  break;
    case 7:   a1=1;  b1=1;  c1=1;  d1=0;  break;
    case 8:   a1=0;  b1=0;  c1=0;  d1=1;  break;
    case 9:   a1=1;  b1=0;  c1=0;  d1=1;  break;
    default:  a1=1;  b1=1;  c1=1;  d1=1;
    break;
  }  
  switch(lookups[number2])
  {
    case 0:   a2=0;  b2=0;  c2=0;  d2=0;  break;
    case 1:   a2=1;  b2=0;  c2=0;  d2=0;  break;
    case 2:   a2=0;  b2=1;  c2=0;  d2=0;  break;
    case 3:   a2=1;  b2=1;  c2=0;  d2=0;  break;
    case 4:   a2=0;  b2=0;  c2=1;  d2=0;  break;
    case 5:   a2=1;  b2=0;  c2=1;  d2=0;  break;
    case 6:   a2=0;  b2=1;  c2=1;  d2=0;  break;
    case 7:   a2=1;  b2=1;  c2=1;  d2=0;  break;
    case 8:   a2=0;  b2=0;  c2=0;  d2=1;  break;
    case 9:   a2=1;  b2=0;  c2=0;  d2=1;  break;
    default:  a2=1;  b2=1;  c2=1;  d2=1;
    break;
  }  
  
  // Write to output pins. - channel 0
  digitalWrite(number0a, a1);
  digitalWrite(number0b, b1);
  digitalWrite(number0c, c1);
  digitalWrite(number0d, d1);

  // Write to output pins. - channel 1
  digitalWrite(number1a, a2);
  digitalWrite(number1b, b2);
  digitalWrite(number1c, c2);
  digitalWrite(number1d, d2);

  // in order to enable the correct tube so it gets the power it needs, all the other bulbs need to be set to low
  if (set == 1) {
    digitalWrite(tube1, LOW);
    digitalWrite(tube2, LOW);
    digitalWrite(tube3, HIGH);
    digitalWrite(tube4, HIGH);
  }
  else {
    digitalWrite(tube1, HIGH);
    digitalWrite(tube2, HIGH);
    digitalWrite(tube3, LOW);
    digitalWrite(tube4, LOW);
  }

  delay(enDelay); // let there be light (for long enough y'know?)
}

void checkSerialClock() {
  if (mySerial.available()) {
    // serial data! make and fill a buffer
    byte outbuffer[6];
    mySerial.readBytes(outbuffer, 6);

    // now assng it
    currentTime.hours = outbuffer[0] * 10 + outbuffer[1];
    currentTime.mins = outbuffer[2] * 10 + outbuffer[3];
    currentTime.secs = outbuffer[4] * 10 + outbuffer[5];

    // clear the serial device buffer
    serialFlush();

    if (DEBUG) {
      Serial.println("H:M:S");
      Serial.print(currentTime.hours);
      Serial.print(":");
      Serial.print(currentTime.mins);
      Serial.print(":");
      Serial.print(currentTime.secs);
    }
  }
}

void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
} 