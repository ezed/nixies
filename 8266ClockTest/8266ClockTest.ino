/*
 * TimeNTP_ESP8266WiFi.ino
 * Example showing time sync to NTP time source
 *
 * This sketch uses the ESP8266WiFi library
 */

#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char ssid[] = "smishsmesh";               //  your network SSID (name)
const char pass[] = "fuzzyfuzzyduck";       // your network password

// NTP Servers:
static const char ntpServerName[] = "au.pool.ntp.org";

// Australian time (non daylight time)
int timeZone = 10;

WiFiUDP Udp;
unsigned int localPort = 8888; // local port to listen for UDP packets

time_t getNtpTime();

void displayTimeToSerial();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);

// members
time_t prevDisplay = 0; // when the digital clock was displayed
bool DEBUG = false;

/*-------- NTP code ----------*/
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

void setup() {
  Serial.begin(19200);
  while (!Serial) ; // Needed for Leonardo only
  delay(250);
  if (DEBUG) {
    Serial.println("TimeNTP Example");
    Serial.print("Connecting to ");
    Serial.println(ssid);
  }

  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    if (DEBUG) {
      Serial.print(".");
    }
  }

  if (DEBUG) {
    Serial.print("IP number assigned by DHCP is ");
    Serial.println(WiFi.localIP());
    Serial.println("Starting UDP");
  }
  Udp.begin(localPort);
  if (DEBUG) {
    Serial.print("Local port: ");
    Serial.println(Udp.localPort());
    Serial.println("waiting for sync");
  }
  setSyncProvider(getNtpTime);
  setSyncInterval(300);
}

void dateTimeCheck() {
  if ((month() > 10) || // more than october
      (month() == 10 && day() >= 6) || // in october and after or including the 6th
      (month() < 4) || // earlier than april
      (month() == 4 && day() <= 5) &&  //in april but before or including the 5th
      timeZone != 11) // do we need to update?
      {
    // daylight savings time!
    timeZone = 11;
    // Force update of the time object
    adjustTime(SECS_PER_HOUR);
    setTime(now());
    return;
  }
  // default
  timeZone = 10;
}

void loop() {
  if (timeStatus() != timeNotSet) {
    dateTimeCheck();
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      displayTimeToSerial();
    }
  }
  delay(10000);
}

void displayTimeToSerial() {
  if (!DEBUG) {
    int hourTemp = hour();
    int minTemp = minute();
    int secondTemp = second();
    byte outbuffer[6] = {
      (hourTemp / 10) & 0xFF,
      (hourTemp % 10) & 0xFF,
      (minTemp / 10) & 0xFF,
      (minTemp % 10) & 0xFF,
      (secondTemp / 10) & 0xFF,
      (secondTemp % 10) & 0xFF,
    };

    Serial.write(outbuffer, 6);
  }
  else {
    printDigits(hour());
    printDigits(minute());
    printDigits(second());
    Serial.print(" ");
    Serial.print(day());
    Serial.print(".");
    Serial.print(month());
    Serial.print(".");
    Serial.print(year());
    Serial.println();
  }
}

void printDigits(int digits) {
  // utility for digital clock display: prints preceding colon and leading 0
  if (digits < 10) Serial.print('0');
  Serial.print(digits);
  Serial.print(":");
}

time_t getNtpTime() {
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  if (DEBUG) {
    Serial.println("Transmit NTP Request");
    Serial.print(ntpServerName);
    Serial.print(": ");
    Serial.println(ntpServerIP);
  }
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      
      if (DEBUG) Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  if (DEBUG) Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address) {
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}
