include <sn_tools.scad> // https://github.com/symmetryninja/openscad-parts/

/** Scripting stuff **/
batch_rendering = false;
if (!batch_rendering) {
  $fn = 90;
  // pillars - 3d printed
  // blue() nc_frame();

  // generates the projection for the laser-cut portion
  nc_layout_cut();

  // visuals for laser cut portions
  // %nc_top_and_bottom(oversize=0);
  // red() nc_sides(oversize=0);
}

/******* Vars *******/
nc_box_int_size = [145, 85, 40,];
nc_pillar_thickness = 10;
nc_panel_thickness = 3;
nc_box_size = [
  X(nc_box_int_size) + 2 * nc_pillar_thickness,
  Y(nc_box_int_size) + 2 * nc_pillar_thickness,
  Z(nc_box_int_size)]
  ;

module nc_frame() {
  D() {
    U() {
      makeRoundedBox(nc_box_size);
    }
    U() {
      // inner box
      ccube(nc_box_int_size);
      // wall cutouts
      ccube(addYZ(nc_box_int_size, nc_pillar_thickness * 2 + 0.1, nc_pillar_thickness * 2 + 0.1));
      ccube(addXZ(nc_box_int_size, nc_pillar_thickness * 2 + 0.1, nc_pillar_thickness * 2 + 0.1));
      // drill-holes
      nc_screws(d=4.1);
      // usb port
      nc_sides(oversize = 0);
    }
  }
}

module nc_layout_cut() {
  projection() {
    T([0, -60, -Z(nc_box_int_size)/2 - 1.5]) nc_bottom();
    T([0, 60, Z(nc_box_int_size)/2 + 1.5]) nc_top();
    T([-(X(nc_box_int_size)/2 + 40), 0, (75 + 1.5)]) Ry() nc_right();
    T([(X(nc_box_int_size)/2 + 40), 0, -(75 + 1.5)]) Ry() nc_left();
    T([0, -(Y(nc_box_int_size) + 60), -50 + 1.5]) Rx() nc_front();
    T([0, (Y(nc_box_int_size) + 60), 50 -1.5]) Rx() nc_rear();
  }
}

module nc_top() {
  D() {
    U() {
        Tz((Z(nc_box_int_size)/2 + nc_panel_thickness/2)) {
          makeRoundedBox(setZ(nc_box_size, 3));
        }
    }
    U() {
      //screws
      nc_screws();

      // sides (for cutout)
      nc_sides(oversize = 0.5);

      // light tubes 13 18, 16.3
      tube_Z = Z(nc_box_int_size)/2;

      tubes = [ [42,7.5,tube_Z], [73,6.9,tube_Z], [108,6.5,tube_Z], [144,7.3,tube_Z],];
      Tx(90.5) Mx(retain=false) place_at_positions(tubes) {
        ccylinder(d=22, h=20);
      }
    }
  }
}
module nc_bottom() {
  D() {
    U() {
        Tz(-(Z(nc_box_int_size)/2 + nc_panel_thickness/2)) {
          makeRoundedBox(setZ(nc_box_size, 3));
        }
    }
    U() {
      //screws
      nc_screws();
      // sides (for cutout)
      nc_sides(oversize = 0.5);
    }
  }
}
module nc_top_and_bottom(oversize=0) {
  nc_bottom();
  nc_top();
}

module nc_screws(d=3.5, h = 50) {
  Mxy() {
    T([77.5, 47.5, 0]) ccylinder(d=d, h = h);
  }
}

module nc_sides(oversize = 0) {
  nc_right(oversize=oversize);
  nc_left(oversize=oversize);
  nc_front(oversize=oversize);
  nc_rear(oversize=oversize);
}

module nc_right(oversize = 0) {
  D() {
    U() {
      Tx(X(nc_box_int_size)/2 + nc_pillar_thickness/2) {
        My() Ty(20) ccube([nc_panel_thickness + oversize, 10 + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([nc_panel_thickness, Y(nc_box_int_size) - 0.1, Z(nc_box_int_size)]);
      }
    }
    U() {
    }
  }
}

module nc_left(oversize = 0) {
  D() {
    U() {
      Tx(-(X(nc_box_int_size)/2 + nc_pillar_thickness/2)) {
        My() Ty(20) ccube([nc_panel_thickness + oversize, 10 + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([nc_panel_thickness, Y(nc_box_int_size) - 0.1, Z(nc_box_int_size)]);
      }
    }
    U() {
    }
  }
}

module nc_front(oversize = 0) {
  D() {
    U() {
      Ty(Y(nc_box_int_size)/2 + nc_pillar_thickness/2) {
        Mx() Tx(50) ccube([10 + oversize, nc_panel_thickness + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([10 + oversize, nc_panel_thickness + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([X(nc_box_int_size) - 0.1, nc_panel_thickness, Z(nc_box_int_size)]);
      }
    }
    U() {
      // cable cutout
      T([X(nc_box_int_size)/2 - 50, Y(nc_box_int_size)/2, -Z(nc_box_int_size)/2 + 15/2]) ccube([15.01, 15.01, 15.01]);
    }
  }
}

module nc_rear(oversize = 0) {
  D() {
    U() {
      Ty(-(Y(nc_box_int_size)/2 + nc_pillar_thickness/2)) {
        Mx() Tx(50) ccube([10 + oversize, nc_panel_thickness + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([10 + oversize, nc_panel_thickness + oversize, Z(nc_box_int_size) + nc_panel_thickness*2 + oversize]);
        ccube([X(nc_box_int_size) - 0.1, nc_panel_thickness, Z(nc_box_int_size)]);
      }
    }
    U() {
    }
  }
}
