#include "Arduino.h"
// #include "ESP8266WiFi.h"
// #include "WiFiUdp.h"
#include <Timer.h> 

// SN74141 : Truth Table
//D C B A #
//L,L,L,L 0
//L,L,L,H 1
//L,L,H,L 2
//L,L,H,H 3
//L,H,L,L 4
//L,H,L,H 5
//L,H,H,L 6
//L,H,H,H 7
//H,L,L,L 8
//H,L,L,H 9

const char* ssid = "";
const char* password = "";

// lookups array of mapping from our wiring numbers to our integer values
const int lookups[10] = { 8, 9, 0, 1, 2, 3, 4, 5, 6, 7 };

const int number0[4] = { 2, 3, 4, 5 };
const int number1[4] = { 6, 7, 8, 9 };
const int tubes[4] = { 10, 11, 12, 13 };

const int tubeSet0[4] = { LOW, LOW, HIGH, HIGH };
const int tubeSet1[4] = { HIGH, HIGH, LOW, LOW };

int binary[4];

// enDelay (ms) to hold a lamp to get some light
const int enDelay = 5;
const long split = 300;

long lastloop = millis() - split;

const int NTP_LOCAL_PORT = 1337;
const int NTP_REMOTE_PORT = 123;
const char* NTP_POOL_ADDRESS = "au.pool.ntp.org";
const int timezone = 11;

const int NTP_PACKET_SIZE = 48;
char ntpRequest[NTP_PACKET_SIZE];
char ntpResponse[NTP_PACKET_SIZE];

// WiFiUDP udp;

typedef struct {
  int hours = 13;
  int mins = 40;
  int secs = 0;
} timeValue;

timeValue currentTime;

Timer timeTimer;

Timer counterUpper;
int temptime = 0;

void countup() {
  temptime += 1;
  temptime = temptime % 10;
  
  displayNumbers(0, temptime, temptime);
  displayNumbers(1, temptime, temptime);
}

void setup() 
{
  // WiFi.begin(ssid, password);
  // Serial.print("Connecting...");
  // while(WiFi.status() != WL_CONNECTED) {
  //   Serial.print(".");
  //   delay(500);
  // }
  // Serial.printf("\nConnected\n");
  // buildNtpRequest();
  // udp.begin(NTP_LOCAL_PORT);

  for (int i = 0; i < 4; i++) 
  {
    pinMode(number0[i], OUTPUT);
    pinMode(number1[i], OUTPUT);
    pinMode(tubes[i], OUTPUT);
  }

  Serial.begin(9600);

  timeTimer.every(60000, updateTime);
  counterUpper.every(500, countup);
}

void updateTime() {
  unsigned long timeInSeconds = getTime();
  int hours = (timeInSeconds / 3600 + timezone) % 12;
  int minutes = timeInSeconds / 60 % 60;

  currentTime.hours = hours;
  currentTime.mins = minutes;
}

void showTime() {
  // int hourTens = currentTime.hours / 10;
  // hourTens = hourTens == 2 ? 5 : hourTens;
  displayNumbers(0, currentTime.hours / 10, currentTime.hours % 10);
  displayNumbers(1, currentTime.mins / 10, currentTime.mins % 10);

}
void showTime2() {
  displayNumbers(0, temptime, temptime);
  displayNumbers(1, temptime, temptime);
}

void buildNtpRequest() {
  // set all bytes in the request to zero
  memset(ntpRequest, 0, NTP_PACKET_SIZE);

  ntpRequest[0] = 0b11100011; // LI, Version, Mode
  ntpRequest[1] = 0; // Stratum or type of clock
  ntpRequest[2] = 6; // Polling interval
  ntpRequest[3] = 0xEC; // Peer clock precision
  // 8 bytes of zero for root delay and root dispersion
  // Then some numbers that we don't know what they mean
  ntpRequest[12] = 49;
  ntpRequest[13] = 0x4E;
  ntpRequest[14] = 49;
  ntpRequest[15] = 52;
}

int convertToBinary(int decimal) {
  binary[0] = decimal % 2 == 0 ? 0 : 1;
  binary[1] = decimal % 4 < 2 ? 0 : 1;
  binary[2] = decimal % 8 < 4 ? 0 : 1;
  binary[3] = decimal < 8 ? 0 : 1;
}

void displayNumbers(int num0, int num1, int set) {

  // need to convert the number to binary to send to SN74141 IC (1)
  convertToBinary(lookups[num0]);
  for (int i = 0; i < 4; i ++) {
    digitalWrite(number0[i], binary[i]);
  }

  convertToBinary(lookups[num1]);
  for (int i = 0; i < 4; i ++) {
    digitalWrite(number1[i], binary[i]);
  }

  if (set) {
    for (int i = 0; i < 4; i++) {
      digitalWrite(tubes[i], tubeSet1[i]);
    }
  } else {
    for (int i = 0; i < 4; i++) {
      digitalWrite(tubes[i], tubeSet0[i]);
    }  
  }
  
  delay(enDelay); // let there be light (for long enough y'know?)

}

unsigned long getTime() 
{
  // udp.beginPacket(NTP_POOL_ADDRESS, NTP_REMOTE_PORT);
  // udp.write(ntpRequest, NTP_PACKET_SIZE);
  // udp.endPacket();

  // // Try 10 times to get a response. If no response, just forget about this loop
  // for (int attempt = 0; attempt < 10; attempt++)
  // {
  //   int packetSize = udp.parsePacket();
  //   if (packetSize > 0)
  //   {
  //     udp.read(ntpResponse, NTP_PACKET_SIZE);
  //     return getTimeInSeconds();
  //   }
  //   delay(100);
  // }
}

unsigned long getTimeInSeconds() 
{
  unsigned long highWord = word(ntpResponse[40], ntpResponse[41]);
  unsigned long lowWord = word(ntpResponse[42], ntpResponse[43]);
  unsigned long seconds = highWord << 16 | lowWord;

  return seconds;
}

void loop() {
  // showTime();
  showTime2();
  counterUpper.update();
}
