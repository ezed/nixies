# Erin's flashy clock light thingy

## The code

The code is split into 2 parts:

1. the esp8266 code
2. the nixie driver code *nixie4.ino*

### the esp8266 code

The code checks NTP every 10 seconds and updates the clock, this is probably a bit of an overkill but it will both ensure the clock is in sync and create enough of a confused pause every now and then on the driver that the numbers will get confused.

Important bits:

- This is where you set the WIFI credentials
- The timezone has been hard-coded to Melbourne time - this is messy and confusing but it works!

The esp8266 baord is a Wimos D1 mini - it acts as a generic esp8266, you'll need the boards manager code for esp8266 to compile it. It's using the serial port to send the current time to the software serial implementation on the Uno board.

### the nixie driver code

Heavily adapted from the robopirate site.

Important bits; with the "2" and "3" pin being broken from the left-most node, the code has been forced to 12-hour time - this is done in the loop function, the code looks like this:

```c++
  currentTime.hours = currentTime.hours % 12;
```

There's a disabled bit of code that will swap out the 2 for a 5 - useful if you're a crazy person - that is in the `showTime()` function.

I was able to get the 2 and 3 pins working but the connection was so weak that the thermal expansion of the glass over a cold night was enough to break the weak solder join. :sad-face:

The `checkSerialClock()` function checks the software serial and updates the time based on the value it got from the ESP.
