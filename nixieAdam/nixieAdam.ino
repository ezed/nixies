// #include <TimeLib.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(15, 14); // RX, TX

// fading transitions sketch for 6-tube IN-17 board with default connections.
// based on 6-tube sketch by Emblazed
// 
// 02/27/2013 - modded for six bulb board, updated flicker fix by Brad L

// SN74141 : Truth Table
//D C B A #
//L,L,L,L 0
//L,L,L,H 1
//L,L,H,L 2
//L,L,H,H 3
//L,H,L,L 4
//L,H,L,H 5
//L,H,H,L 6
//L,H,H,H 7
//H,L,L,L 8
//H,L,L,H 9

   //Initialize serial and wait for port to open:


  /*
  Note you need 115200 baud /r/n for newlines

  got these here:
  https://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/#AT+CIPSTA

  // at commands to setup 8266
  // set the mode to client
  AT+CWMODE=1 

  // connect to access point
  AT+CWJAP="SSID","PASSWORD"

  // get ip
  AT+CIFSR

  // connect to time server
  AT+CIPSTART="UDP","pool.ntp.org",123"
  */

 /// this may actually do the whole thing...

//  https://symmetry.ninja/time_AU.php Also just does the hh:mm:ss

// Defines
  long MINS = 60;         // 60 Seconds in a Min.
  long HOURS = 60 * MINS; // 60 Mins in an hour.
  long DAYS = 24 * HOURS; // 24 Hours in a day. > Note: change the 24 to a 12 for non military time.
  long runTime = 0;       // Time from when we started.

  // default time sets. clock will start at 12:34:00.  This is so we can count the correct order of tubes.
  long clockHourSet = 12;
  long clockMinSet  = 34;

  int HourButtonPressed = false;
  int MinButtonPressed = false;
  float fadeMax = 5.0f;
  float fadeStep = 1.0f;
  int NumberArray[6]={0,1,2,3,4,5};
  int currNumberArray[6]={0,0,0,0,0,0};
  float NumberArrayFadeInValue[6]={0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
  float NumberArrayFadeOutValue[6]={8.0f,8.0f,8.0f,8.0f,8.0f,8.0f};

  int upperHours = 1;
  int lowerHours = 2;
  int upperMins = 3;
  int lowerMins = 4;
  int lowerSeconds = 5;
  int upperSeconds = 6;

  int secondsOffset = 0;

  char buffer[30];
  char* inputString;
  char outputMessage[30];

  // SN74141 (1)
  int ledPin_0_a = 2;                
  int ledPin_0_b = 3;
  int ledPin_0_c = 4;
  int ledPin_0_d = 5;

  // SN74141 (2)
  int ledPin_1_a = 6;                
  int ledPin_1_b = 7;
  int ledPin_1_c = 8;
  int ledPin_1_d = 9;

  // anode pins
  int ledPin_a_1 = 10;
  int ledPin_a_2 = 11;
  int ledPin_a_3 = 12;
  int ledPin_a_4 = 13;

  bool DEBUG = true;

  void setup() {
    mySerial.begin(19200); 
    if (DEBUG) Serial.begin(9600); 

    pinMode(ledPin_0_a, OUTPUT);
    pinMode(ledPin_0_b, OUTPUT);
    pinMode(ledPin_0_c, OUTPUT);
    pinMode(ledPin_0_d, OUTPUT);
    
    pinMode(ledPin_1_a, OUTPUT);
    pinMode(ledPin_1_b, OUTPUT);
    pinMode(ledPin_1_c, OUTPUT);
    pinMode(ledPin_1_d, OUTPUT);
    
    pinMode(ledPin_a_1, OUTPUT);
    pinMode(ledPin_a_2, OUTPUT);
    pinMode(ledPin_a_3, OUTPUT);
  }

  void SetSN74141Chips( int num2, int num1 ) {
    int a,b,c,d;
    
    // set defaults.
    a=0;b=0;c=0;d=0; // will display a zero.
    
    // Load the a,b,c,d.. to send to the SN74141 IC (1)
    switch( num1 )
    {
      case 0: a=0;b=0;c=0;d=0;break;
      case 1: a=1;b=0;c=0;d=0;break;
      case 2: a=0;b=1;c=0;d=0;break;
      case 3: a=1;b=1;c=0;d=0;break;
      case 4: a=0;b=0;c=1;d=0;break;
      case 5: a=1;b=0;c=1;d=0;break;
      case 6: a=0;b=1;c=1;d=0;break;
      case 7: a=1;b=1;c=1;d=0;break;
      case 8: a=0;b=0;c=0;d=1;break;
      case 9: a=1;b=0;c=0;d=1;break;
      default: a=1;b=1;c=1;d=1;
      break;
    }  
    
    // Write to output pins.
    digitalWrite(ledPin_0_a, a);
    digitalWrite(ledPin_0_b, b);
    digitalWrite(ledPin_0_c, c);
    digitalWrite(ledPin_0_d, d);

    // Load the a,b,c,d.. to send to the SN74141 IC (2)
    switch( num2 )
    {
      case 0: a=0;b=0;c=0;d=0;break;
      case 1: a=1;b=0;c=0;d=0;break;
      case 2: a=0;b=1;c=0;d=0;break;
      case 3: a=1;b=1;c=0;d=0;break;
      case 4: a=0;b=0;c=1;d=0;break;
      case 5: a=1;b=0;c=1;d=0;break;
      case 6: a=0;b=1;c=1;d=0;break;
      case 7: a=1;b=1;c=1;d=0;break;
      case 8: a=0;b=0;c=0;d=1;break;
      case 9: a=1;b=0;c=0;d=1;break;
      default: a=1;b=1;c=1;d=1;
      break;
    }
    
    // Write to output pins
    digitalWrite(ledPin_1_a, a);
    digitalWrite(ledPin_1_b, b);
    digitalWrite(ledPin_1_c, c);
    digitalWrite(ledPin_1_d, d);
  }

  void DisplayFadeNumberString() {

    // Anode channel 1 - numerals 0,3
    SetSN74141Chips(currNumberArray[0],currNumberArray[3]);   
    digitalWrite(ledPin_a_2, HIGH);   
    delay(NumberArrayFadeOutValue[0]);
    SetSN74141Chips(NumberArray[0],NumberArray[3]);   
    delay(NumberArrayFadeInValue[0]);
    digitalWrite(ledPin_a_2, LOW);
    
    // Anode channel 2 - numerals 1,4
    SetSN74141Chips(currNumberArray[1],currNumberArray[4]);   
    digitalWrite(ledPin_a_3, HIGH);   
    delay(NumberArrayFadeOutValue[1]);
    SetSN74141Chips(NumberArray[1],NumberArray[4]);   
    delay(NumberArrayFadeInValue[1]);
    digitalWrite(ledPin_a_3, LOW);
    
    // Anode channel 3 - numerals 2,5
    SetSN74141Chips(currNumberArray[2],currNumberArray[5]);   
    digitalWrite(ledPin_a_4, HIGH);   
    delay(NumberArrayFadeOutValue[2]);
    SetSN74141Chips(NumberArray[2],NumberArray[5]);   
    delay(NumberArrayFadeInValue[2]);
    digitalWrite(ledPin_a_4, LOW);
    
    // Loop thru and update all the arrays, and fades.
    for( int i = 0 ; i < 6 ; i ++ )
    {
      if( NumberArray[i] != currNumberArray[i] )
      {
        NumberArrayFadeInValue[i] += fadeStep;
        NumberArrayFadeOutValue[i] -= fadeStep;
    
        if( NumberArrayFadeInValue[i] >= fadeMax )
        {
          NumberArrayFadeInValue[i] = 0.0f;
          NumberArrayFadeOutValue[i] = fadeMax;
          currNumberArray[i] = NumberArray[i];
        }
      }
    }  
  }

  void checkSerialClock() {
    if (mySerial.available()) {
      // serial data! make and fill a buffer
      byte outbuffer[6];
      mySerial.readBytes(buffer, 6);

      // now assng it
      upperHours = buffer[0];
      lowerHours = buffer[1];
      upperMins = buffer[2];
      lowerMins = buffer[3];
      upperSeconds = buffer[4];
      lowerSeconds = buffer[5];

      // clear the serial device buffer
      serialFlush();

      if (DEBUG) {
        Serial.println("H h:M m:S s");
        Serial.print(upperHours);
        Serial.print(" ");
        Serial.print(lowerHours);
        Serial.print(":");
        Serial.print(upperMins);
        Serial.print(" ");
        Serial.print(lowerMins);
        Serial.print(":");
        Serial.print(upperSeconds);
        Serial.print(" ");
        Serial.println(lowerSeconds);
      }

      int newSeconds = (upperSeconds * 10) + lowerSeconds;
      secondsOffset = newSeconds - ((millis() / 1000 ) % 60);
    }
  }

  void serialFlush(){
    while(Serial.available() > 0) {
      char t = Serial.read();
    }
  } 

  bool minuteIncremented = false; // potentially a dumb way to make sure the increment is like a clock instead of like a crazy person

  void loop() {
    // Get milliseconds.
    runTime = millis();
    
    // Check Clock serial
    checkSerialClock();
    
    // Get time in seconds.
    long seconds  = (runTime / 1000 + secondsOffset) % 60;

    if (seconds == 0) {
      if (minuteIncremented == false) {
        lowerMins += 1;
        minuteIncremented = true;
        if (lowerMins > 9) upperMins += 1;
        if (upperMins > 5) lowerHours +=1;
        if (lowerHours > 9) upperHours += 1;
      }
    }
    else {
      minuteIncremented = false;
    }

    // Little johny tables
    // upperHours = upperHours % 3;
    // lowerHours = lowerHours % 10; 
    // upperMins = upperMins % 6;
    // lowerMins = lowerMins % 10;
    // lowerSeconds = seconds % 10;
    // upperSeconds = ((seconds - lowerSeconds)/10) % 6;

    // if (upperHours == 2) upperHours = 5;
    
    // Fill in the Number array used to display on the tubes.
    NumberArray[5] = upperHours;
    NumberArray[4] = lowerHours;
    NumberArray[3] = upperMins;
    NumberArray[2] = lowerMins;
    NumberArray[1] = upperSeconds;
    NumberArray[0] = lowerSeconds;

    // Display.
    DisplayFadeNumberString();
  }
